# Cstomerify

A ruby client for the [Customerify.com](http://customerify.com) [event API](https://customerify.com/api/docs/index.html).

## Installation

Add this line to your application's Gemfile:

    gem 'customerify'

And then execute:

    $ bundle

Or install it yourself:

    $ gem install customerify

## Usage

